package com.mitocode.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mitocode.model.Venta;

@Repository
public interface IVentaDao extends JpaRepository<Venta, Integer> {

}
